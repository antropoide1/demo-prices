package kos.prices.demo;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import kos.prices.demo.application.PricesController;
import kos.prices.demo.core.ports.PricesApplicationPort;
import kos.prices.demo.core.ports.PricesPersistencePort;
import kos.prices.demo.persistence.repository.PricesRepository;

@SpringBootTest
class DemoApplicationTests {
	
	@Autowired
	private PricesController pricesController;

	@Autowired
	private PricesApplicationPort pricesApplicationPort;
	
	@Autowired
	private PricesPersistencePort pricesPersistencePort;
	
	@Autowired
	private PricesRepository pricesRepository;

	@Test
	void contextLoads() {
		
		// We choose the main interfaces implied fin the context loading.
		assertThat(pricesController).isNotNull();
		assertThat(pricesApplicationPort).isNotNull();
		assertThat(pricesPersistencePort).isNotNull();
		assertThat(pricesRepository).isNotNull();
		
	}

}
