package kos.prices.demo.web;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import kos.prices.demo.utils.AppConstants;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class HttpRequestTest {
	
	@Value(value="${local.server.port}")
	private int port;
	
	@Autowired
	private TestRestTemplate testRestTemplate;

	@Test
	void applicationIsUpAskingActuator() {
		
		assertThat(
				this.testRestTemplate.getForObject("http://localhost:" + port + "/actuator/health", String.class))
		.contains("UP");
	}
	
	@Test
	void applicationDateIsInRange() {
		String applicationDate = "2020-06-14T11:00:00";
		
		assertThat(
				this.testRestTemplate.getForObject(testUrlBuilder(applicationDate ) ,String.class))
		.contains("35.50");
	}
	
	@Test
	void applicationDateIsOutOfMaxBound() {
		String applicationDate = "2022-06-14T11:00:00";
		
		ResponseEntity<String> response = 
				this.testRestTemplate.getForEntity(testUrlBuilder(applicationDate ),String.class);

		Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}
	
	@Test
	void applicationDateIsOutOfMinBound() {
		String applicationDate = "2017-06-14T11:00:00";
		
		ResponseEntity<String> response = 
				this.testRestTemplate.getForEntity(testUrlBuilder(applicationDate ) ,String.class);

		Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}
	
	@Test
	void requiredTest1() {
		String applicationDate = "2020-06-14T10:00:00";
		
		assertThat(
				this.testRestTemplate.getForObject(testUrlBuilder(applicationDate ) ,String.class))
		.contains("35.50");
	}
	
	@Test
	void requiredTest2() {
		String applicationDate = "2020-06-14T16:00:00";
		
		assertThat(
				this.testRestTemplate.getForObject(testUrlBuilder(applicationDate ) ,String.class))
		.contains("25.45");
	}
	
	@Test
	void requiredTest3() {
		String applicationDate = "2020-06-14T21:00:00";
		
		assertThat(
				this.testRestTemplate.getForObject(testUrlBuilder(applicationDate ) ,String.class))
		.contains("35.50");
	}
	
	@Test
	void requiredTest4() {
		String applicationDate = "2020-06-15T10:00:00";
		
		assertThat(
				this.testRestTemplate.getForObject(testUrlBuilder(applicationDate ) ,String.class))
		.contains("30.50");
	}
	
	@Test
	void requiredTest5() {
		String applicationDate = "2020-06-16T21:00:00";
		
		assertThat(
				this.testRestTemplate.getForObject(testUrlBuilder(applicationDate ) ,String.class))
		.contains("38.95");
	}
	
	
	private String testUrlBuilder(String theDateHourTime) {
		
		StringBuilder sb = new StringBuilder();
		
		return sb
		.append(AppConstants.URL_LOCALHOST)
		.append(port)
		.append(AppConstants.URL_ENDPOINT)
		.append(AppConstants.URL_PARAM_1_BRAND)
		.append(AppConstants.URL_PARAM_2_APP_DATE)
		.append(theDateHourTime)
		.append(AppConstants.URL_PARAM_PRODUCT_ID)
		.toString();
	}
}
