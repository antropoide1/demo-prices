package kos.prices.demo.persistence.repository;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;

import kos.prices.demo.persistence.entity.Price;

public interface PricesRepository extends JpaRepository<Price, Long>{
	
	ArrayList<Price> findAllByProductId(String productId);
	
	ArrayList<Price> findAllByProductIdAndEndDateAfter(String productId, Date referenceDate);
	
	ArrayList<Price> findAllByProductIdAndStartDateBefore(String productId, Date referenceDate);
	

}
