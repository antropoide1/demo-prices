package kos.prices.demo.persistence.entity;

import java.math.BigDecimal;
import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name= "prices")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Price {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="prices_seq_gen")
	@SequenceGenerator(name="prices_seq_gen", sequenceName="prices_seq", allocationSize = 1)
	private Long id;
	
	@Column(name="brand_id")
	private Integer brandId;
	
	@Column(name="start_date")
	private Date startDate;
	
	@Column(name="end_date")
	private Date endDate;
	
	@Column(name="price_list")
	private Integer priceList;
	
	@Column(name="product_id")
	private String productId;
	
	@Column(name="priority")
	private Integer priority;
	
	@Column(name="price")
	private BigDecimal productPrice;
	
	@Column(name="currency")
	private String currency;
	
	

}
