package kos.prices.demo.persistence.mapper;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import kos.prices.demo.core.dto.PriceDto;
import kos.prices.demo.persistence.entity.Price;

@Mapper(componentModel = "spring")
public interface PriceMapperPersistence {

	PriceMapperPersistence INSTANCE=Mappers.getMapper(PriceMapperPersistence.class);
	
	List<PriceDto> listPriceToListPriceDto(List<Price> list);
	
}
