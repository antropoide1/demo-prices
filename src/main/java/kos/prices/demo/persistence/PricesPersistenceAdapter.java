package kos.prices.demo.persistence;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kos.prices.demo.core.dto.PriceDto;
import kos.prices.demo.core.dto.PriceRequestDto;
import kos.prices.demo.core.ports.PricesPersistencePort;
import kos.prices.demo.persistence.entity.Price;
import kos.prices.demo.persistence.mapper.PriceMapperPersistence;
import kos.prices.demo.persistence.repository.PricesRepository;

@Service
public class PricesPersistenceAdapter implements PricesPersistencePort {

	@Autowired
	private PricesRepository pricesRepository;
	
	@Override
	public List<PriceDto> getPrice(PriceRequestDto priceRequest) {
		
		List<Price> listPrices = pricesRepository.findAllByProductId(priceRequest.getProductId());
		
		return PriceMapperPersistence.INSTANCE.listPriceToListPriceDto(listPrices);
	}

	@Override
	public List<PriceDto> findAllByProductIdAndEndDateAfter(PriceRequestDto priceRequest) {
		List<Price> listPrices = pricesRepository.findAllByProductIdAndEndDateAfter(priceRequest.getProductId(), priceRequest.getApplicationDate());
		
		return PriceMapperPersistence.INSTANCE.listPriceToListPriceDto(listPrices);
	}

	@Override
	public List<PriceDto> findAllByProductIdAndStartDateBefore(PriceRequestDto priceRequest) {
		List<Price> listPrices = pricesRepository.findAllByProductIdAndStartDateBefore(priceRequest.getProductId(), priceRequest.getApplicationDate());
		
		return PriceMapperPersistence.INSTANCE.listPriceToListPriceDto(listPrices);
	}

}
