package kos.prices.demo.utils;

public class AppConstants {
	
	// Paths
	public static final String PRICE_PATH = "/price-to-apply";
	public static final String URL_LOCALHOST = "http://localhost:";
	public static final String URL_ENDPOINT = "/price-to-apply";
	public static final String URL_PARAM_1_BRAND= "?brandId=1";
	public static final String URL_PARAM_2_APP_DATE= "&applicationDate=";
	public static final String URL_PARAM_PRODUCT_ID = "&productId=35455";
//	public static final String
//	public static final String
	
	  private AppConstants () {
		    throw new IllegalStateException("Utility class");
		  }

}
