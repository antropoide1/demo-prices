package kos.prices.demo.application;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kos.prices.demo.core.dto.ProductPriceResponseDto;
import kos.prices.demo.utils.AppConstants;

@RestController
public interface PricesController {

	@GetMapping(AppConstants.PRICE_PATH)
	ResponseEntity<ProductPriceResponseDto> getPrice(@RequestParam Integer brandId, 
			@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME, pattern = "yyyy-MM-dd'T'HH:mm:ss") Date  applicationDate, 
			@RequestParam String productId) throws Exception;

}
