package kos.prices.demo.application;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import kos.prices.demo.core.dto.PriceRequestDto;
import kos.prices.demo.core.dto.ProductPriceResponseDto;
import kos.prices.demo.core.ports.PricesApplicationPort;

@Service
public class PricesApiAdapter implements PricesController {
	
	@Autowired
	private PricesApplicationPort pricesApplicationPort;

	@Override
	public ResponseEntity<ProductPriceResponseDto> getPrice(Integer brandId, Date applicationDate,String productId) throws Exception {
		
		PriceRequestDto priceRequestDto = new PriceRequestDto(brandId, applicationDate, productId);
		
		Optional<ProductPriceResponseDto> price = pricesApplicationPort.getPrice(priceRequestDto);
		
		if(price.isEmpty()) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		
		return new ResponseEntity<>(price.get(), HttpStatus.OK);
	}

}
