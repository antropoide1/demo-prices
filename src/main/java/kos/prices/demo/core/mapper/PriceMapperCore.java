package kos.prices.demo.core.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import kos.prices.demo.core.dto.PriceDto;
import kos.prices.demo.core.dto.ProductPriceResponseDto;


@Mapper(componentModel = "spring")
public interface PriceMapperCore {
	
	 PriceMapperCore  INSTANCE = Mappers.getMapper( PriceMapperCore .class);
	 
	 ProductPriceResponseDto priceDtoToProductPriceResponseDto(PriceDto priceDto);
	 
	 
}
