package kos.prices.demo.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kos.prices.demo.core.dto.PriceDto;
import kos.prices.demo.core.dto.PriceRequestDto;
import kos.prices.demo.core.dto.ProductPriceResponseDto;
import kos.prices.demo.core.mapper.PriceMapperCore;
import kos.prices.demo.core.ports.PricesApplicationPort;
import kos.prices.demo.core.ports.PricesPersistencePort;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PricesServiceImp implements PricesApplicationPort {
	
	@Autowired
	private PricesPersistencePort pricesPersistencePort;

	@Override
	public Optional<ProductPriceResponseDto> getPrice(PriceRequestDto priceRequest) throws Exception {
		/*
		// Step 0 Stub for checking the connection between application and persistence
			// Find all by brandId & productId 
		List<PriceDto> pricesByRequestedBrandAndProduct =  pricesPersistencePort.getPrice(priceRequest);
			// If list is empty return not found: Optional.empty
		if(pricesByRequestedBrandAndProduct.isEmpty()) return Optional.empty();
		
			// else send any register, let's say the first of the list.
		PriceDto anyPrice= pricesByRequestedBrandAndProduct.get(0);
		//HeroMapper.INSTANCE.listHeroToListHeroDto(theHeroesList.get());
		return Optional.of(PriceMapperCore.INSTANCE.priceDtoToProductPriceResponseDto(anyPrice));
		*/
		
		// Step 1:  Check if the data is out of the bounds of the maximum end_date or minimum start_date
		Optional<List<PriceDto>> productIdAndStartDateBefore = getApplicationDataRelatedPricesInsideOfBounds(priceRequest);
		if(productIdAndStartDateBefore.isEmpty()) return  Optional.empty(); // ApplicationDate out of bounds
		
		// Step 2: Filter in the List productIdAndStartDateBefore, the elements with end_date higher than applicationDate	
		Optional<List<PriceDto>> productIdAndStartDateBeforeAndEndDataAfter = getElementsWithEndDateHigherThanApplicationDate(
				priceRequest, productIdAndStartDateBefore.get());
		if(productIdAndStartDateBeforeAndEndDataAfter.isEmpty()) return  Optional.empty(); // There isn't any interval that contains the applicationDate.
		
		// Step 3: productIdAndStartDateBeforeAndEndDataAfter elements, picking the elements with max priority values.		
		List<PriceDto> priorityMax = getTheRecordsSortedByMaxPriority(productIdAndStartDateBeforeAndEndDataAfter.get());
		
		//Step 4: Pick the max start_date from priorityMax list. 	
		PriceDto finalPrice = getTheFinalPriceFromList(priorityMax);
		
		//Step 5: map the PriceDto to ProductPriceResponseDto and return it
		return Optional.of(PriceMapperCore.INSTANCE.priceDtoToProductPriceResponseDto(finalPrice));

}

	// Step 1:  Check if the data is out of the bounds of the maximum end_date or minimum start_date
	private Optional<List<PriceDto>> getApplicationDataRelatedPricesInsideOfBounds(PriceRequestDto priceRequest) {
		if(pricesPersistencePort.findAllByProductIdAndEndDateAfter(priceRequest).isEmpty()) {
			log.info(">>>>>>>>>>>>>>>>>>>>>>>> Date out of the max range");
			return Optional.empty();
		}
		
		List<PriceDto> productIdAndStartDateBefore = pricesPersistencePort.findAllByProductIdAndStartDateBefore(priceRequest);
		if(productIdAndStartDateBefore.isEmpty()) {
			log.info(">>>>>>>>>>>>>>>>>>>>>>>> Date out of the min range");
			return Optional.empty();
		}
		
		log.info(">>>>>>>>>>>>>>>>>>>>>>>> Date inside of the range");
		return Optional.of(productIdAndStartDateBefore);
	}

	// Step 2: Filter in the List productIdAndStartDateBefore, the elements with end_date higher than applicationDate	
	private Optional<List<PriceDto>> getElementsWithEndDateHigherThanApplicationDate(PriceRequestDto priceRequest,
			List<PriceDto> productIdAndStartDateBefore) {
		List<PriceDto> productIdAndStartDateBeforeAndEndDataAfter= productIdAndStartDateBefore.stream()
																																		.filter(e -> 
																																							e.getEndDate().after( priceRequest.getApplicationDate()) ||
																																							e.getEndDate().equals( priceRequest.getApplicationDate())
																																				).toList();
		if(productIdAndStartDateBeforeAndEndDataAfter.isEmpty()) {
			log.info(">>>>>>>>>>>>>>>>>>>>>>>> Application end_date out of the end_date  range, for start_dates list before the applicationDate");
			return Optional.empty();
		}
		// The applicationDate now is inside of the interval represented by the start and date of the elements list. 
		log.info(">>>>>>>>>>>>>>>>>>>>>>>> productIdAndStartDateBeforeAndEndDataAfter: " + productIdAndStartDateBeforeAndEndDataAfter.toString() );
		return Optional.of(productIdAndStartDateBeforeAndEndDataAfter);
	}

	// Step 3: productIdAndStartDateBeforeAndEndDataAfter elements, picking the elements with max priority values.
	// Sort the list
	private List<PriceDto> getTheRecordsSortedByMaxPriority(List<PriceDto> productIdAndStartDateBeforeAndEndDataAfter) {

		List<PriceDto> productIdAndStartDateBeforeAndEndDataAfterSortedByPriorityAl = getAllTheListElementsOrderedByPriority(
				productIdAndStartDateBeforeAndEndDataAfter);
		
		return getTheElementsWithMaximumPriorityLevel(
				productIdAndStartDateBeforeAndEndDataAfterSortedByPriorityAl);
	}
	// Step 3.2
	private List<PriceDto> getTheElementsWithMaximumPriorityLevel(
			List<PriceDto> productIdAndStartDateBeforeAndEndDataAfterSortedByPriorityAl) {
		//Pick the max priority elements.
		ListIterator<PriceDto> listIterator = productIdAndStartDateBeforeAndEndDataAfterSortedByPriorityAl.listIterator();
		List<PriceDto> priorityMax = new ArrayList<>();
		Integer maxPriority = productIdAndStartDateBeforeAndEndDataAfterSortedByPriorityAl.get(0).getPriority();
		
		while(listIterator.hasNext()) {
			PriceDto thePrice = listIterator.next();
			if(Objects.equals(thePrice.getPriority(), maxPriority)) {
				priorityMax .add(thePrice);
			}
		}
		return priorityMax;
	}
	// Step 3.1
	private List<PriceDto> getAllTheListElementsOrderedByPriority(
			List<PriceDto> productIdAndStartDateBeforeAndEndDataAfter) {
		List<PriceDto> productIdAndStartDateBeforeAndEndDataAfterSortedByPriority= productIdAndStartDateBeforeAndEndDataAfter.stream()
		.sorted(
				(p1, p2) -> p1.getPriority().compareTo(p2.getPriority())
				).toList();
		List <PriceDto> productIdAndStartDateBeforeAndEndDataAfterSortedByPriorityAl = new ArrayList<>(productIdAndStartDateBeforeAndEndDataAfterSortedByPriority);
		Collections.reverse(productIdAndStartDateBeforeAndEndDataAfterSortedByPriorityAl);
		return productIdAndStartDateBeforeAndEndDataAfterSortedByPriorityAl;
	}

	//Step 4: Pick the max start_date from priorityMax list. 
	// The element with the max start_date (more recent) between the max priority elements will be picked as outcome. 
	private PriceDto getTheFinalPriceFromList(List<PriceDto> priorityMax) {

		priorityMax=priorityMax.stream()
				.sorted(
						(p1, p2) -> p1.getStartDate().compareTo(p2.getStartDate())
						).toList();
		
		PriceDto finalPrice = priorityMax.get(0);
		log.info(">>>>>>>>>>>>>>>>>>>>>>>> Final price:  " + finalPrice.toString());
		return finalPrice;
	}
}
