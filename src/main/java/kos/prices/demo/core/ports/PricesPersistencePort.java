package kos.prices.demo.core.ports;

import java.util.ArrayList;
import java.util.List;

import kos.prices.demo.core.dto.PriceDto;
import kos.prices.demo.core.dto.PriceRequestDto;

public interface PricesPersistencePort {
	
	List<PriceDto> getPrice(PriceRequestDto priceRequestDto);
	
	List<PriceDto> findAllByProductIdAndEndDateAfter(PriceRequestDto priceRequest);
	
	List<PriceDto> findAllByProductIdAndStartDateBefore(PriceRequestDto priceRequest);



}
