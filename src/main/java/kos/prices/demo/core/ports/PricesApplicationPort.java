package kos.prices.demo.core.ports;

import java.util.Optional;

import kos.prices.demo.core.dto.PriceRequestDto;
import kos.prices.demo.core.dto.ProductPriceResponseDto;

public interface PricesApplicationPort {
	
	Optional<ProductPriceResponseDto> getPrice(PriceRequestDto priceRequestDto) throws Exception;

}
