package kos.prices.demo.core.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@Builder
public class PriceRequestDto implements Serializable {

	private static final long serialVersionUID = -5926933894709735487L;
	
	@NotEmpty
	private Integer brandId;
	@NotEmpty
	private Date applicationDate;
	@NotEmpty
	private String productId;

}
