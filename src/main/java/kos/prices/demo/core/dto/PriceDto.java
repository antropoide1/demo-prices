package kos.prices.demo.core.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PriceDto  implements Serializable{

	private static final long serialVersionUID = 4323597591281451319L;

	private Long id;
	private Integer brandId;
	private Date startDate;
	private Date endDate;
	private Integer priceList;
	private String productId;
	private Integer priority;
	private BigDecimal productPrice;
	private String currency;
}
