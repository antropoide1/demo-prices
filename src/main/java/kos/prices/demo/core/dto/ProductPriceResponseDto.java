package kos.prices.demo.core.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductPriceResponseDto implements Serializable {

	private static final long serialVersionUID = -5026933894709735480L;
	
	private Integer brandId;
	private Date startDate;
	private Date endDate;
	private Integer priceList;
	private String productId;
	private BigDecimal productPrice;
	private String currency;

}
