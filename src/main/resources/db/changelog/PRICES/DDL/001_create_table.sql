--liquibase formatted sql

--changeset antropoide:create-prices-table
create  sequence heroes_seq START WITH 5;
create table prices(
       id bigint primary key,
       brand_id int not null,
       start_date datetime not null,
       end_date datetime not null,
       price_list int not null,
       product_id varchar(64) not null,
       priority int not null,
       price decimal (10,2) not null,
       currency char(3) not null
);
