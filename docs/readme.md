
# Table of Contents

1.  [About the project](#org2ad9660)
2.  [About the model](#org55b732d)
3.  [The hexagonal design](#org8b961a2)
4.  [API](#org9586337)
    1.  [OpenApi definition available:](#orgba2f2a0)
    2.  [Funcionality first check. cURL is your friend.](#orgf534e42)
        1.  [Custom out of bounds checking cases.](#org61d05ea)
        2.  [Required cases endpoints](#orgf57c190)
5.  [Setup](#org1678773)
6.  [What's next:](#org871ecc1)


<a id="org2ad9660"></a>

# About the project

In the company's e-commerce database, we have the PRICES table that reflects the final price (pvp) and the rate that applies to a product in a chain between certain dates. Below is an example of the table with the relevant fields:

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-right" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">ID</th>
<th scope="col" class="org-right">BRAND<sub>ID</sub></th>
<th scope="col" class="org-left">START<sub>DATE</sub></th>
<th scope="col" class="org-left">END<sub>DATE</sub></th>
<th scope="col" class="org-right">PRICE<sub>LIST</sub></th>
<th scope="col" class="org-right">PRODUCT<sub>ID</sub></th>
<th scope="col" class="org-right">PRIORITY</th>
<th scope="col" class="org-right">PRICE</th>
<th scope="col" class="org-left">CURR</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">1</td>
<td class="org-right">1</td>
<td class="org-left">2020-06-14 00:00:00</td>
<td class="org-left">2020-12-31 23:59:59</td>
<td class="org-right">1</td>
<td class="org-right">35455</td>
<td class="org-right">0</td>
<td class="org-right">35.5</td>
<td class="org-left">EUR</td>
</tr>


<tr>
<td class="org-right">2</td>
<td class="org-right">1</td>
<td class="org-left">2020-06-14 15:00:00</td>
<td class="org-left">2020-06-14 18:30:00</td>
<td class="org-right">2</td>
<td class="org-right">35455</td>
<td class="org-right">1</td>
<td class="org-right">25.45</td>
<td class="org-left">EUR</td>
</tr>


<tr>
<td class="org-right">3</td>
<td class="org-right">1</td>
<td class="org-left">2020-06-15 00:00:00</td>
<td class="org-left">2020-06-15 11:00:00</td>
<td class="org-right">3</td>
<td class="org-right">35455</td>
<td class="org-right">1</td>
<td class="org-right">30.5</td>
<td class="org-left">EUR</td>
</tr>


<tr>
<td class="org-right">4</td>
<td class="org-right">1</td>
<td class="org-left">2020-06-15 16:00:00</td>
<td class="org-left">2020-12-31 23:59:59</td>
<td class="org-right">4</td>
<td class="org-right">35455</td>
<td class="org-right">1</td>
<td class="org-right">38.95</td>
<td class="org-left">EUR</td>
</tr>
</tbody>
</table>

**It is requested:**

-   Build an application/service in SpringBoot that provides a query rest endpoint such that:
    -   Accept as input parameters: application date, product identifier, string identifier.
    -   Return as output data: product identifier, brand identifier, price list to apply, application dates and final price to apply.


<a id="org55b732d"></a>

# About the model

Right with the project description, the project model is just a simple table as example, that in relational db environment may have this design:


<a id="org8b961a2"></a>

# The hexagonal design

The application schema is an one module hexagonal project in order to make easy accomplish with the current common deploy specifications in microservices, like containerization, orchestration, deployment as lambda function, etc&#x2026;  

![img](./docs/app_architecture_plot.drawio.png)

-   


<a id="org9586337"></a>

# API

-   The API, right with the description has only one endpoint:
    -   /price-to-apply


<a id="orgba2f2a0"></a>

## OpenApi definition available:

    http://localhost:8080/swagger-ui/index.html      


<a id="orgf534e42"></a>

## Funcionality first check. cURL is your friend.


<a id="org61d05ea"></a>

### Custom out of bounds checking cases.

-   See logs in console.

1.  date on range

        url="http://localhost:8080/price-to-apply?brandId=1&applicationDate=2020-06-14T11:00:00&productId=35455" && curl -v "$url"

2.  date out of range (max)

        url="http://localhost:8080/price-to-apply?brandId=1&applicationDate=2022-06-14T11:00:00&productId=35455" && curl -v "$url"

3.  date out of range (min)

        url="http://localhost:8080/price-to-apply?brandId=1&applicationDate=2015-06-14T11:00:00&productId=35455" && curl -v "$url" 


<a id="orgf57c190"></a>

### Required cases endpoints

1.  Test 1: petición a las 10:00 del día 14 del producto 35455   para la brand 1 (ZARA)

    Request:
    
        url="http://localhost:8080/price-to-apply?brandId=1&applicationDate=2020-06-14T10:00:00&productId=35455" && curl -v "$url"
    
    Expected return:

2.  Test 2: petición a las 16:00 del día 14 del producto 35455   para la brand 1 (ZARA)

    Request:
    
        url="http://localhost:8080/price-to-apply?brandId=1&applicationDate=2020-06-14T16:00:00&productId=35455" && curl -v "$url"
    
    Expected return:
    
        {"brandId":1,"startDate":"2020-06-14T15:00:00.000+02:00","endDate":"2020-06-14T18:30:00.000+02:00","priceList":2,"productId":"35455","productPrice":25.45,"currency":"EUR"}

3.  Test 3: petición a las 21:00 del día 14 del producto 35455   para la brand 1 (ZARA)

    Request:
    
        url="http://localhost:8080/price-to-apply?brandId=1&applicationDate=2020-06-14T21:00:00&productId=35455" && curl -v "$url"
    
    Expected return:
    \#+begin<sub>src</sub> json
    {"brandId":1,"startDate":"2020-06-14T00:00:00.000+02:00","endDate":"2020-12-31T23:59:59.000+01:00","priceList":1,"productId":"35455","productPrice":35.50,"currency":"EUR"}#+end<sub>src</sub>    

4.  Test 4: petición a las 10:00 del día 15 del producto 35455   para la brand 1 (ZARA)

    Request:
    
        url="http://localhost:8080/price-to-apply?brandId=1&applicationDate=2020-06-15T10:00:00&productId=35455" && curl -v "$url"
    
    Expected return:
    
        {"brandId":1,"startDate":"2020-06-15T00:00:00.000+02:00","endDate":"2020-06-15T11:00:00.000+02:00","priceList":3,"productId":"35455","productPrice":30.50,"currency":"EUR"}

5.  Test 5: petición a las 21:00 del día 16 del producto 35455   para la brand 1 (ZARA)

    Request:
    
        url="http://localhost:8080/price-to-apply?brandId=1&applicationDate=2020-06-16T21:00:00&productId=35455" && curl -v "$url"
    
    Expected return:
    
        {"brandId":1,"startDate":"2020-06-15T16:00:00.000+02:00","endDate":"2020-12-31T23:59:59.000+01:00","priceList":4,"productId":"35455","productPrice":38.95,"currency":"EUR"}


<a id="org1678773"></a>

# Setup

    #cd to the project root first.
    mvn test
    mvn clean install
    mvn spring-boot:run


<a id="org871ecc1"></a>

# What's next:

-   Bussiness logic 2nd refactoring.
-   Testing by mocking.
-   Some other additional features.
    -   Docker
    -   GitLab pipelines for CI and CD.

